-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 28. Feb 2020 um 12:08
-- Server-Version: 10.3.22-MariaDB-1
-- PHP-Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ts_admin`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sessions`
--

CREATE TABLE `sessions` (
  `key` varchar(128) NOT NULL,
  `user` int(11) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `time` int(11) NOT NULL,
  `os` varchar(256) NOT NULL,
  `browser` varchar(256) NOT NULL,
  `remember_me` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `token` varchar(128) NOT NULL,
  `emailauthtoken` varchar(128) NOT NULL,
  `resetrequestdate` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `resetToken` varchar(128) NOT NULL,
  `displayname` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `token`, `emailauthtoken`, `resetrequestdate`, `date`, `group`, `resetToken`, `displayname`, `active`) VALUES
(1, 'Bixilon', 'bixilon@bixilon.de', '$2y$10$PLaqh/CSJInSyk5JYC1s3uIHLieeYmqbJ.ioJrpM9qLq6SlmOCCwG', 'ABCDEF', '', 0, 1569506719, 1, '', 'Moritz', 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `key` (`key`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
