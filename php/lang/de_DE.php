<?php
$language['login_header'] = "Bitte Anmelden!";
$language['placeholder_username'] = "Benutzername";
$language['placeholder_password'] = "Passwort";
$language['placeholder_2fa'] = "2FA Anmelde Code";
$language['login_remember_me'] = "Angemeldet bleiben";
$language['login_sign_in'] = "Anmelden";

$language['login_error_field_not_set'] = "Alle Felder müssen ausgefüllt werden";
$language['login_wrong'] = "Benutzername, Passwort oder 2FA Code falsch!";



$language['logout'] = "Abmelden";






$language['copyright'] = "&copy; Bixilon 2020";