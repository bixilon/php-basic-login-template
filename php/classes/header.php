<?php

if (!isset($title))
    $title = "Unbekannt";

//imports

//lang (de_DE is the only one for now)
require_once $rd . "php/lang/de_DE.php";

echo '<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title>' . $name . ' - ' . $title . '</title>';

//req login css
if (isset($css_login) && $css_login)
    echo '<link href="lib/css/floating-labels.css" rel="stylesheet">
          <link href="/lib/css/bootstrap-4/bootstrap.min.css" rel="stylesheet">
          <link href="/lib/css/login.css" rel="stylesheet">';
else
    echo '
    <!-- Bootstrap core CSS -->
    <link href="/lib/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/lib/css/jumbotron.css" rel="stylesheet">';

echo '</head>';

//if this is true, nothing will get displayed here
if (!isset($custom) || !$custom) {
//navbar
    echo '<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
        <form action="/logout.php" class="form-inline my-2 my-lg-0">
          <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">' . $language['logout'] . '</button>
        </form>
      </div>
    </nav>';

    echo '<main role="main">';
}