<?php
$rd = "../../";


class Updater
{
    public $error = 0;
    public $log = ""; //html log to display

    function __construct($currentversion = null/*Alte version bzw aktuelle */, $newversion = "master" /*master*/)
    {
        //init
        global $new, $old, $rd, $log;
        $log .= 'Initializing updater...';

        if ($currentversion == null) {
            $currentversion = exec('cd ' . $rd . ' &&git rev-parse --short HEAD');
        }
        $old = $currentversion;


        $new = $newversion;

        //log
        $log .= '<span class="text-success">finished</span><br>';
    }

    function getLog()
    {
        global $log;
        return $log;
    }

    function update()
    {
        global $debug, $log, $error, $old, $new;

        $log .= 'Starting Update from version <b>' . $old . '</b> to <b>' . $new . '</b><br>';
        if (!$debug) {
            //If not in debug mode, don't fetch from git...otherwise all files are getting overwritten (if not commited)
            $this->fetchFromGit();
        }


        if ($error != 0)
            return;

        $log .= '<span class="text-success">Update finished!</span><br>';

    }

    function fetchFromGit()
    {
        global $new, $old, $rd, $log, $error;
        $log .= 'Fetching files from git...';
        exec('cd ' . $rd . ' && git fetch', $output, $ret);
        if ($ret == 0) {
            $log .= '<span class="text-success">ok</span><br>';
            //continue
        } else {
            //failed to fetch new files...update is useless: aborting
            $log .= '<span class="text-danger">failed(' . $ret . ')</span>!<br>';
            for ($i = 0; $i < count($output); $i++) {
                $log .= $output[$i] . '<br>';
            }
            $log .= '<br>';
            fail();
        }

        if ($error != 0)
            return;
        //no error until now... checking if already up-to-date
        $log .= 'Checking for update...';
        exec('cd ' . $rd . ' && git log -n 1 origin/master --pretty=format:"%h"', $output, $ret);
        if ($ret == 0) {
            $log .= '<span class="text-success">ok</span><br>';
            if ($output[0] == $old) {
                //no update there
                $log .= "No update available!<br>";
                return;
            } else
                $log .= "An update is available...Updating!<br>";
            //continue
        } else {
            //failed to fetch new files...update is useless: aborting
            $log .= '<span class="text-danger">failed(' . $ret . ')</span>!<br>';
            for ($i = 0; $i < count($output); $i++) {
                $log .= $output[$i] . '<br>';
            }
            $log .= '<br>';
            fail();
        }

        //

        //set head to new version
        $log .= 'Reset head to ' . $new . '...';

        $branch = "origin/master";
        if ($new != "master")
            $branch = $new;
        exec('cd ' . $rd . ' && git reset --hard ' . $branch, $output, $ret);

        if ($ret == 0) {
            $log .= '<span class="text-success">ok</span><br>';
            //continue
        } else {
            //failed to fetch new files...update is useless: aborting
            $log .= '<span class="text-danger">failed(' . $ret . ')</span>!<br>';
            for ($i = 0; $i < count($output); $i++) {
                $log .= $output[$i] . '<br>';
            }
            $log .= '<br>';
            fail();
        }

        if ($error != 0)
            return;

        //performing mysql updates
        $log .= 'Migrating MySLQ tables...';
        $s = $this->mySQLMigration();
        if ($s == 1) {
            //skipped; Nothing to do
            $log .= 'skipped<br>';
        } else if ($s == 0) {
            //ok
            $log .= '<span class="text-success">ok</span><br>';
        } else {
            $log .= '<span class="text-danger">failed</span><br>';
            fail();
        }

        //ToDo: Minify JS and CSS


    }

    function mySQLMigration()
    {
        global $old, $new;
        exec("git rev-list --ancestry-path " . $old . ".." . $new, $commits, $ret);
        if ($ret != 0) {
            return 2;
        }

        $done = 0;
        for ($i = 0; $i < count($commits); $i++) {
            if ($this->migrateCommit($commits[$i]))
                $done++;
        }
        if ($this->migrateCommit("master"))
            $done++;
        if ($done == 0)
            return 1;
        return 0;
    }

    function migrateCommit($commit)
    {
        global $db;
        switch ($commit) {
        }
        return false;
    }

    function fail($failcode = null)
    {
        //Runs when Update fails.
        global $log, $error, $rd, $old, $new;
        $log .= '<span class="text-danger">Critical error occurred. Aborting the update. See log for details!</span>';
        if ($failcode != null)
            $error = $failcode;
        else
            $error = 1;

        //rollback

        $log .= 'Reverting changes...';
        exec("git rev-list --ancestry-path " . $old . ".." . $new, $commits, $ret);
        if ($ret == 0) {
            for ($i = 0; $i < count($commits); $i++) {
                $this->revertCommit($commits[$i]);
            }
            $this->revertCommit("master");
        } else
            $log .= '<span class="text-danger">failed!</span>';


        exec('cd ' . $rd . ' && git reset --hard ' . $old, $output, $ret);
        if ($ret == 0)
            $log .= '<span class="text-success">ok</span>';
        else
            $log .= '<span class="text-danger">failed!</span>';
    }

    function revertCommit($commit)
    {
        global $db;
        switch ($commit) {

        }
    }
}